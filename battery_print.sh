#!/bin/bash

#This script is to add cpu temp and battery percentage to the status bar in tmux
# on the right side.

$batt_level
$discharging
$cpu_temp

# checking to see if the battery status is charging or discharging
discharging=$(cat /sys/class/power_supply/BAT0/status)
# getting value for battery life
batt_level=$(cat /sys/class/power_supply/BAT0/capacity)
cpu_temp=$(cat /sys/devices/virtual/thermal/thermal_zone0/temp)
# value from temp is temp * 1000, so must divide to get actual value
cpu_temp=$((cpu_temp / 1000))

if [ $discharging = "Charging" ]
then
	echo "CPU_Temp:"$cpu_temp \| "Battery:"$batt_level% "I feel the power!" \|
else
	echo "CPU_Temp:"$cpu_temp \| "Battery:"$batt_level% $discharging \|
fi

